define("gallery/dropdownx/1.0.0/dropdownx", [ "jquery" ], function(require, exports, module) {
    var jQuery = require("jquery");

if (jQuery) (function ($) {

    $.extend($.fn, {
        dropdownx: function (method, data) {

            switch (method) {
                case 'show':
                    show(null, $(this));
                    return $(this);
                case 'hide':
                    hide();
                    return $(this);
                case 'attach':
                    return $(this).attr('data-dropdownx', data);
                case 'detach':
                    hide();
                    return $(this).removeAttr('data-dropdownx');
                case 'disable':
                    return $(this).addClass('dropdownx-disabled');
                case 'enable':
                    hide();
                    return $(this).removeClass('dropdownx-disabled');
            }

        }
    });

    function show(event, object) {

        var trigger = event ? $(this) : object,
			dropdownx = $('#'+trigger.attr('data-dropdownx')),
			isOpen = trigger.hasClass('dropdownx-open');

        // In some cases we don't want to show it
        if (event) {
            if ($(event.target).hasClass('dropdownx-ignore')) return;

            event.preventDefault();
            event.stopPropagation();
        } else {
            if (trigger !== object.target && $(object.target).hasClass('dropdownx-ignore')) return;
        }
        hide();

        if (isOpen || trigger.hasClass('dropdownx-disabled')) return;

        // Show it
        trigger.addClass('dropdownx-open');

        dropdownx
			.data('dropdownx-trigger', trigger)
			.show();

        // Position it
        position();

        // Trigger the show callback
        dropdownx
			.trigger('show', {
			    dropdownx: dropdownx,
			    trigger: trigger
			});

    }

    function hide(event) {

        // In some cases we don't hide them
        var targetGroup = event ? $(event.target).parents().addBack() : null;

        // Are we clicking anywhere in a dropdownx?
        if (targetGroup && targetGroup.is('.dropdownx')) {
            // Is it a dropdownx menu?
            if (targetGroup.is('.dropdownx-menu')) {
                // Did we click on an option? If so close it.
                if (!targetGroup.is('A')) return;
            } else {
                // Nope, it's a panel. Leave it open.
                return;
            }
        }

        // Hide any dropdownx that may be showing
        $(document).find('.dropdownx:visible').each(function () {
            var dropdownx = $(this);
            dropdownx
				.hide()
				.removeData('dropdownx-trigger')
				.trigger('hide', { dropdownx: dropdownx });
        });

        // Remove all dropdownx-open classes
        $(document).find('.dropdownx-open').removeClass('dropdownx-open');

    }

    function position() {

        var dropdownx = $('.dropdownx:visible').eq(0),
			trigger = dropdownx.data('dropdownx-trigger'),
			hOffset = trigger ? parseInt(trigger.attr('data-horizontal-offset') || 0, 10) : null,
			vOffset = trigger ? parseInt(trigger.attr('data-vertical-offset') || 0, 10) : null;

        if (dropdownx.length === 0 || !trigger) return;

        // Position the dropdownx relative-to-parent...
        if (dropdownx.hasClass('dropdownx-relative')) {
            dropdownx.css({
                left: dropdownx.hasClass('dropdownx-anchor-right') ?
					trigger.position().left - (dropdownx.outerWidth(true) - trigger.outerWidth(true)) - parseInt(trigger.css('margin-right')) + hOffset :
					trigger.position().left + parseInt(trigger.css('margin-left')) + hOffset,
                top: trigger.position().top + trigger.outerHeight(true) - parseInt(trigger.css('margin-top')) + vOffset
            });
        } else {
            // ...or relative to document
            dropdownx.css({
                left: dropdownx.hasClass('dropdownx-anchor-right') ?
					trigger.offset().left - (dropdownx.outerWidth() - trigger.outerWidth()) + hOffset : trigger.offset().left + hOffset,
                top: trigger.offset().top + trigger.outerHeight() + vOffset
            });
        }
    }

    $(document).on('click.dropdownx', '[data-dropdownx]', show);
    $(document).on('click.dropdownx', hide);
    $(window).on('resize', position);

})(jQuery);
});