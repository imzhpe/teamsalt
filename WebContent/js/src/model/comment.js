define(function(require, exports, module) {

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');

	var Comment = Backbone.Model.extend({
		add : function(params, success) {
			var url = '/comment/addf';
			Util.query.p(url, params, function(result) {
				success(result.other);
			});
		}
	});
	module.exports = Comment;
})