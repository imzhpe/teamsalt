/**
 * 改变任务状态widget 2013-10-16 08:03:32
 */
define(function(require, exports, module) {

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');
	var DropdownView = require('../ui-component/dropdownView');
	var taskStatusHtml = require('./tpl/task-status.html');

	var TaskStatusWidget = function(curTarget, taskUuid) {
		this.curTarget = curTarget;
		this.taskUuid = taskUuid;
		this.init();
	}
	TaskStatusWidget.prototype.init = function() {
		this.taskStatusDropdownView = new DropdownView(this.curTarget, Util.tpl
				.t(taskStatusHtml, {}));
		this.taskStatusDropdownView.open();
		this.taskStatusDropdownView.getRoot().find('li>a').on('click',
				$.proxy(this.changeTaskStatus, this));
	};
	TaskStatusWidget.prototype.changeTaskStatus = function(e) {
		var self = this;
		var taskStatus = $(e.currentTarget).attr("data-status");
		var task = Util.Models.get('task');
		var params = {
				uuid:this.taskUuid,
				field:'status',
				value:taskStatus
		};
		
		task.updateField(params,function(data){
			var t = data.task;
			self.curTarget.removeClass();
			if(t.status==0){
				self.curTarget.addClass('icon-circle icon-large task-status');
			}else if(t.status==1){
				self.curTarget.addClass('icon-check-empty icon-large task-status');
			}else if(t.status==2){
				self.curTarget.addClass('icon-check icon-large task-status');
			}
			
		});
		
	};

	module.exports = TaskStatusWidget;
})