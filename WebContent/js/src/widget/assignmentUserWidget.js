define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');

	var PopboxView = require('../ui-component/popboxView');

    // 指定分配人模板
    var assignmentUserHtml = require('./tpl/assignment-user.html');

    var self = this;

	var AssignmentUserWidget = function(curTarget,taskId){
		this.curTarget = curTarget;
		
		this.init();
	}
	AssignmentUserWidget.prototype.init = function() {
		Util.Models.get('user').getAllUser(function(result){
			self.userList = result;
		});		
		this.assignmentUserPopbox = new PopboxView(this.curTarget,'aa','bottom',Util.tpl.t(assignmentUserHtml, this.userList));
        this.assignmentUserPopbox.open();
        this.assignmentUserPopbox.getPopboxRoot().find('li').on('click', $.proxy(this.selectUser, this));
	};
	AssignmentUserWidget.prototype.selectUser = function(e) {
		var selectUser = $(e.currentTarget);
		if(selectUser.find('i').hasClass('icon-ok')){
			selectUser.find('i').removeClass('icon-ok');
		}else{
			selectUser.find('i').addClass('icon-ok')
		}
		
	};
	
	module.exports = AssignmentUserWidget;
})