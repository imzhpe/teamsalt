/*
* listpanel 任务列表分类的任务列表
*/
define(function(require, exports, module){

	var $ = require('jquery');
	
    var taskListHtml = require('./tpl/taskList.html');

	var TaskListWidget = function(taskList){
		this.taskList = taskList;
		this.init();
	};
	TaskListWidget.prototype.init = function() {
		this.contentHtml=Util.tpl.t(taskListHtml, this.taskList)
	};
	TaskListWidget.prototype.html = function() {
		return this.contentHtml;
	};
	
	module.exports = TaskListWidget;
})