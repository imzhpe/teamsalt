define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');


	//使用artTemplate模板引擎
	var DropdownView = require('../ui-component/dropdownView');

    // 指定分配人模板
    var taskListsHtml = require('./tpl/task-lists.html');


	var AaskListsWidget = function(curTarget,taskId){
		this.curTarget = curTarget;
		this.init();
	}
	AaskListsWidget.prototype.init = function() {
		var self = this;
		Util.Models.get('tasklist').getTasklistList(Util.curProjectId,function(result){
			self.tasklistList = result;
			self.aaskPriorityDropdownView = new DropdownView(self.curTarget,Util.tpl.t(taskListsHtml, self.tasklistList));
	        self.aaskPriorityDropdownView.open();
	        self.aaskPriorityDropdownView.getRoot().find('li>a').on('click', $.proxy(self.selectUser, self));
		});			

	};
	AaskListsWidget.prototype.selectUser = function(e) {
		var curTasklistName = $(e.currentTarget).text();
		var curTasklistId = $(e.currentTarget).attr('data-id');
		this.curTarget.val(curTasklistName)
		this.curTarget.prev().val(curTasklistId)
	};
	
	module.exports = AaskListsWidget;
})