/**
 * @author zhaopeng
 * @datetime 2013-09-06
 * @description 项目所有使用的工具类
 */
define(function(require, exports, module) {
	var $ = require('jquery');
	var _ = require('underscore');
	var XDate = require('xdate');
	var template = require('template');
	var templateExt = require('template-extensions');
	var Map = require('../object/map');
	var Notify = require('notify');
	/**
	 * 日期工具
	 */
	var Date = {
		// 获取当前时间,eg. 2013-09-07 00:17:38
		now : function() {
			var xdate = new XDate();
			return xdate.toString('yyyy-MM-dd hh:mm:ss');
		},
		// 获取当前时间,eg. 2013-09-07
		nowDate : function() {
			var xdate = new XDate();
			return xdate.toString('yyyy-MM-dd');
		},
		// 将 2013-09-07 00:17:38 格式化为  2013-09-07
		getDate : function(dateTime) {
			if(_.isUndefined(dateTime) || _.isNull(dateTime) || _.isEmpty(dateTime)  ){
				return "";
			}
			var xdate = new XDate(dateTime);
			return xdate.toString('yyyy-MM-dd');
		},
		getTime : function(dateTime) {
			if(_.isUndefined(dateTime) || _.isNull(dateTime) || _.isEmpty(dateTime)  ){
				return 0;
			}
			var xdate = new XDate(dateTime);
			return xdate.getTime();
		}
	}
	//常用工具
	var Kit = {
		isNull:function(obj){
			return (_.isUndefined(obj) || _.isNull(obj) || _.isEmpty(obj) );
		}
	}

	/**
	 * 查询工具
	 */
	var Query = {
		g : function(url, params, success) {
			this._ajax(url, 'get', params, success);
		},
		p : function(url, params, success) {
			this._ajax(url, 'post', params, success);
		},
		j : function(url, params, success) {
			this._ajax(url, 'jsonp', params, success);
		},
		_ajax : function(url, type, data, success) {
			//url='/test4/dummy'+url+'.json';
			jQuery.ajax({
				url : url,
				type : type,
				data : data,
				success : function(result) {
					if (result.code == 0) {
						success(result);
					} else {
						seajs.log(result.message);
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		}
	}

	/**
	 * 模板工具
	 */
	var Tpl = {
		t : function(tpl, data) {
			var render = template.compile(tpl);
			var tempData = {
				d : data
			}
			var html = render(tempData);
			return html;
		}
	}

	/**
	 * 提醒工具
	 */
	var Notice = {
		// 在右上角进行提醒
		n : function(message) {
			if (!$('body').find('#notifications').hasClass('notifications')) {
				$('body')
						.append(
								'<div id="notifications" class="notifications top-right"></div>');
			}
			$('.top-right').notify({
				message : {
					text : message
				}
			}).show();
			// 3秒后消息
			setTimeout(function() {
				$('.top-right').find('.alert').hide('slow').remove();
			}, 3000);

			$('.top-right').find('.close').trigger('click');// 放在单击事件前面,解决需要点击两次才可以关闭的问题
			$('.top-right').find('.close').on('click', function() {
				$(this).parent().hide('slow').remove();
			});
		}
	}
	var Util = {
		date : Date,
		query : Query,
		kit : Kit,
		tpl : Tpl,
		Views: new Map(), //作为所有view的map集合,
		Models: new Map(), //作为所有model的map集合,
		notice : Notice,
	}

	module.exports = Util;
})
