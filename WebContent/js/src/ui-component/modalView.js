/**
 * @author zhaopeng
 * @datetime 2013-08-27
 * @description 在detailpanel页面加载完成后,加载公共组件
 */
define(function(require, exports, module){

	var $ = require('jquery');
	// Bootstrap 插件 
	var Bootstrap = require('bootstrap');
	var modalTpl = '<div class="modal fade" role="dialog" > <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h4 class="modal-title"></h4> </div> <div class="modal-body"> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button> <button type="button" class="btn btn-primary">确定</button> </div> </div> </div> </div>';

	var ModalView = function(title,html,callback){
		this.root = $(modalTpl);
		this.title=title;
		this.html=html;
		this.callback=callback;
		this.init();
	} 
	ModalView.prototype.init = function() {
		this.root.find('.modal-title').text(this.title);
		this.root.find('.modal-body').html(this.html);
		this.root.appendTo('body');			

	};
	ModalView.prototype.open = function() {
		this.root.modal('show');		
	};

	ModalView.prototype.close = function() {
		this.root.modal('hide');		
	};	
	ModalView.prototype.getRoot = function() {
		return this.root;
	};	
	module.exports = ModalView;
})
