/**
 * @author zhaopeng
 * @datetime 2013-08-27
 * @description 在detailpanel页面加载完成后,加载公共组件
 */
define(function(require, exports, module){

	var $ = require('jquery');
	// Bootstrap 插件 
	var Bootstrap = require('bootstrap');
	var popbox = require('popbox');

	var PopboxView = function(selecter,title,placement,html,callback){
		this.root = $(selecter);
		this.title=title;
		this.placement=placement;
		this.html=html;
		this.callback=callback;
		this.init();
	} 
	PopboxView.prototype.init = function() {
		var a = this.root.popbox({
			container:'body',
			title:this.title,
			placement:this.placement,
			html:true,
			content:this.html
		});
		// a.getPopboxID();
	};
	PopboxView.prototype.open = function() {
		if (!this.root.hasClass('open-popbox')) {
			this.root.popbox('show');
			this.root.addClass('open-popbox');
		};

		this.popboxRoot=$('#'+this.root.attr('popboxId'));
		this.popboxRoot.find('.popbox-close').on('click', $.proxy(this.close, this));
		this.popboxRoot.find('.btn-default').on('click', $.proxy(this.close, this));
		this.popboxRoot.find('.btn-success').on('click', $.proxy(this.callback, this));
	};

	PopboxView.prototype.close = function() {
		this.root.popbox('hide');
		
	};	
	PopboxView.prototype.getPopboxRoot = function() {
		return this.popboxRoot;		
	};
	module.exports = PopboxView;
})
