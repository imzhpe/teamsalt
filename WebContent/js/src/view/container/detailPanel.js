define(function(require, exports, module) {

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');

	// 使用artTemplate模板引擎
    var tags = require('tags');
    var PopboxView = require('../../ui-component/popboxView');
    var Bootbox = require('bootbox');
    var Notify = require('notify');
    var Datepicker = require('datepicker');

	var Comment = require('../../model/comment');

    // 详情模板
    var detailPanelHtml = require('./tpl/detail-panel.html');

    // 详情模板
    var newCommentHtml = require('./tpl/new-comment.html');

    var AssignmentUserWidget = require('../../widget/assignmentUserWidget');
    var TaskPriorityWidget = require('../../widget/taskPriorityWidget');
    var TastListsWidget = require('../../widget/tastListsWidget');
    var TaskUsersWidget = require('../../widget/taskUsersWidget');


	var DetailPanel = Backbone.View.extend({
		el: '#detail-panel',
		model:null,
        events:{
            'click .task-users':'assignmentUser',
            'click .task-priority':'taskPriority',
            'click .task-lists':'taskLists',
            'click .assign-date':'taskDate',
            'keypress .task-comment': 'comment',
            'click .task-delete-btn':'delete',
            'click .task-save-btn':'save'
        },
		initialize: function() {
			
		},
        save:function(e){
            e.preventDefault();
            e.stopPropagation();
            var self = this;
            var task = Util.Models.get('task');
            var taskForm = this.$el.find('form[name="task-form"]');
            task.update(taskForm.serialize(),function(result){
            	self.$el.trigger('updateTask',result);//更新列表中的task
            });
        },
        delete:function(e){
            e.preventDefault();
            e.stopPropagation();
        	var self = this;
            bootbox.confirm("确定删除任务吗?", function(result) {
            	if(result){
            		var task = Util.Models.get('task');
            		var taskId = self.$el.find('input[name="task.uuid"]').val();
            		task.delete(taskId,function(result){
            			self.$el.html('');
            			self.$el.trigger('deleteTask',result);//删除列表中的task
            		});
            	}
            }); 
        },
        taskDate:function(e){
            e.preventDefault();
            e.stopPropagation();
            var taskDate  = $(e.currentTarget);
            taskDate.datepicker('show').on('changeDate', function(ev) {
                var d = new Date(ev.date);
                taskDate.datepicker('hide');
            });
        },
        taskLists:function(e){
            e.preventDefault();
            e.stopPropagation();
            var taskLists  = $(e.currentTarget);
            new TastListsWidget(taskLists);
        },
        taskPriority:function(e){
            e.preventDefault();
            e.stopPropagation();
            var taskPriority  = $(e.currentTarget);
            new TaskPriorityWidget(taskPriority);
        },
        assignmentUser:function(e){
            e.preventDefault();
            e.stopPropagation();
       
            var taskUsers  = $(e.currentTarget);
            new TaskUsersWidget(taskUsers);
        },
        comment:function(e){
            if (e.keyCode==10 && e.ctrlKey) {
                e.preventDefault();
                e.stopPropagation();
                var comment = Util.Models.get('comment');
                var commentTarget  = $(e.currentTarget);
                var self = this; 
                var commentForm =  this.$el.find('form[name="comments-form"]');
                comment.add(commentForm.serialize(),function(result){
                	var commentLists = self.$el.find('#comments-lists');
                	self.$el.find('#comments-lists').append(Util.tpl.t(newCommentHtml, result)).hide().slideToggle('slow');                	
                	commentTarget.val("");
                    var height = commentLists[0].scrollHeight;
                    commentLists.animate({
                        scrollTop:height
                    });
                });

            };
        },
        /**
		 * 任务详情
		 */
        getTask:function(params){
        	var task = Util.Models.get('task');
            task.getTaskById(
            	params,
                $.proxy(this.loadTask,this)
            );                
        },
        loadTask:function(task){
            this.$el.html(Util.tpl.t(detailPanelHtml, task));
            this.$el.trigger('updateCommon');
            $(".tm-input").tagsManager();
        }
	});

	module.exports = DetailPanel;
})