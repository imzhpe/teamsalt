/**
 * @author zhaopeng
 * @datetime 2013-08-27
 * @description 在detailpanel页面加载完成后,加载公共组件
 */
define(function(require, exports, module){

	var $ = require('jquery');
	var _ = require('underscore');
	var Backbone = require('backbone');
	// Bootstrap 插件 
	var Bootstrap = require('bootstrap');

	// 右键插件
	var Contextmenu = require('contextmenu');

	//滚动条插件
	var perfectScrollbar = require('perfect-scrollbar');

	//日期插件
	var datepicker = require('datepicker');

	//select插件
	var select = require('select');
	//select插件
//	var PopboxView = require('popboxView');

	var CommonView={
		init:function () {
			this._scrollbar();
			this._tooltip();
			this._contextmenu();
			this._datepicker();
		},
		_scrollbar:function () {
	        $('#comments-lists').perfectScrollbar({
	          wheelSpeed: 20,
	          wheelPropagation: true
	        });
	       
	        $('#list-wrapper').perfectScrollbar({
	          wheelSpeed: 20,
	          wheelPropagation: true
	        });

	        $('#sidebar').perfectScrollbar({
	          wheelSpeed: 20,
	          wheelPropagation: true
	        });			
		},
		_tooltip:function () {
			$('#list-panel a').tooltip({
				placement:"top"
			});
			$('#sidebar a').tooltip({
				placement:"right"
			});
			
		},
		_contextmenu:function () {
			//屏蔽右键
			// document.oncontextmenu=new Function("event.returnValue=false;");
   //   		document.onselectstart=new Function("event.returnValue=false;");

			// $('#sidebar-tasklistlist').contextmenu({
		 //        target: '#context-menu-tasklist',
		 //        onItem: function(e, item) {
		 //          alert($(item).text());
		 //        }
		 //    });     
		},
		_datepicker:function () {
				
	

		}
	}

	module.exports = CommonView;
})
