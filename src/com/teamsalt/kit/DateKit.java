package com.teamsalt.kit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import jodd.util.StringUtil;

public class DateKit {
	private static Calendar calendar = Calendar.getInstance();
	private static SimpleDateFormat shortFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	private static SimpleDateFormat longFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	/**
	 * 日期增加天数后得到新日期
	 * 
	 * @param dateStr
	 * @param addDay
	 * @return
	 */
	public static String addDate(String dateStr, int addDay) {
		Date date = null;
		try {
			date = longFormat.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, addDay);
		date = c.getTime();
		String newDate = longFormat.format(date);
		return newDate;
	}

	/**
	 * 时间比较
	 * 
	 * @param dateA
	 * @param dateB
	 * @return
	 */
	public static boolean compareDate(String dateA, String dateB) {
		boolean flag = false;
		try {
			flag = longFormat.parse(dateA).getTime() > longFormat.parse(dateB)
					.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return flag;
	}

	public static String getNowDate() {
		return shortFormat.format(new Date());
	}

	public static String getDate(Date date) {
		return date == null ? null : shortFormat.format(date);
	}

	public static String getNowDateTime() {
		return longFormat.format(new Date());
	}

	public static String getDateTime(Date date) {
		return date == null ? null : longFormat.format(date);
	}

	public static Date getDate(String date) throws ParseException {
		if (StringUtil.isEmpty(date)) {
			return null;
		}
		return shortFormat.parse(date);
	}

	public static Date getDateTime(String dateTime) throws ParseException {
		if (StringUtil.isEmpty(dateTime)) {
			return null;
		}
		return longFormat.parse(dateTime);
	}

	public static int getYear() {
		return calendar.get(Calendar.YEAR);
	}

	public static int getMonth() {
		return calendar.get(Calendar.MONTH);
	}

	public static int getDay() {
		return calendar.get(Calendar.DATE);
	}

	public static int getHour() {
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMinute() {
		return calendar.get(Calendar.MINUTE);
	}

	public static int getSecond() {
		return calendar.get(Calendar.SECOND);
	}
}
