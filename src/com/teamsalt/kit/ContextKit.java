package com.teamsalt.kit;

import javax.servlet.http.HttpServletRequest;

import com.teamsalt.common.TeamsaltConstants;
import com.teamsalt.model.User;

/**
 * 
 * @ClassName: ContextKit
 * @Description: 用户登录的上下文工具类
 * @author zhaopeng
 * @date 2013-8-7 上午10:15:00
 * 
 */
public class ContextKit {

    /**
     * 
     * @Title: getCurrentUser
     * @Description: 获得当前登录用户
     * @param @param request
     * @param @return 设定文件
     * @return Users 返回类型
     * @throws
     */
    public static User getCurrentUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(TeamsaltConstants.LOGIN_USER);
    }

    public static String getCurrentUserUuid(HttpServletRequest request) {
        return getCurrentUser(request).getStr("uuid");
    }

    public static String getCurrentUserUsername(HttpServletRequest request) {
        return getCurrentUser(request).getStr("username");
    }

    public static String getCurrentUserEmail(HttpServletRequest request) {
        return getCurrentUser(request).getStr("email");
    }
}
