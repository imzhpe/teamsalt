package com.teamsalt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jfinal.core.Controller;
import com.teamsalt.common.Message;
import com.teamsalt.kit.ContextKit;
import com.teamsalt.kit.DateKit;
import com.teamsalt.kit.UuidKit;
import com.teamsalt.model.Tasklist;

/**
 * 
 * @ClassName: TodolistController
 * @Description: todolist
 * @author zhaopeng
 * @date 2013-8-11 下午3:50:43
 * 
 */
public class TasklistController extends Controller {
	private static final Logger log = Logger
			.getLogger(TasklistController.class);

	/**
	 * 
	 * @Title: index
	 * @Description: 获取project的tasklist
	 * @param 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	public void index() {
		String projectUuid = getPara(0);
		List list = Tasklist.dao.getByProjectUuid(projectUuid);
		Map map = new HashMap();
		map.put("list", list);
		Message msg = new Message(0, "添加成功", map);
		renderJson(msg);
	}

	/**
	 * 
	 * @Title: savef
	 * @Description: 保存tasklists
	 * @param 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	public void savef() {
		Tasklist tasklist = getModel(Tasklist.class);
		tasklist.set("uuid", UuidKit.uuid());
		tasklist
				.set("creat_uuid", ContextKit.getCurrentUserUuid(getRequest()));
		tasklist.set("create_time", DateKit.getNowDateTime());

		tasklist.save();

		Map map = new HashMap();
		map.put("tasklists", tasklist);
		Message msg = new Message(0, "添加成功", map);

		renderJson(msg);
	}
	/**
	 * 
	 * @Title: index
	 * @Description: 获取project的tasklist
	 * @param 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	public void getTasklistListByProject() {
		String projectUuid = getPara(0);
		List list = Tasklist.dao.getByProjectUuid(projectUuid);
		Map map = new HashMap();
		map.put("tasklistList", list);
		Message msg = new Message(0, "添加成功", map);
		renderJson(msg);
	}
}
