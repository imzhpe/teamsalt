package com.teamsalt.controller;

import java.util.List;

import org.apache.log4j.Logger;

import com.jfinal.aop.Before;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.GET;
import com.jfinal.ext.interceptor.POST;
import com.teamsalt.common.TeamsaltConstants;
import com.teamsalt.interceptor.CheckLoginInterceptor;
import com.teamsalt.kit.ContextKit;
import com.teamsalt.kit.PwdKit;
import com.teamsalt.model.Project;
import com.teamsalt.model.User;

/**
 * 
 * @ClassName: IndexController
 * @Description: 整站首页,负责整个页面的跳转
 * @author zhaopeng
 * @date 2013-6-21 下午2:57:31
 * 
 */
public class IndexController extends Controller {
    private static final Logger log = Logger.getLogger(IndexController.class);

    @Before(GET.class)
    @ClearInterceptor(ClearLayer.ALL)
    public void index() {
    	renderHtml("<a href='/login'>login</a>");
    }

    @Before(GET.class)
    public void home() {
    	Project firstProject = Project.dao.getFirstProject(ContextKit.getCurrentUserUuid(getRequest()));
    	redirect("/main.html#main/"+firstProject.getStr("uuid"));
    }

    @Before({ GET.class, CheckLoginInterceptor.class })
    @ClearInterceptor(ClearLayer.ALL)
    public void login() {
        render("/index.jsp");
    }

    @Before(GET.class)
    public void logout() {
        removeSessionAttr(TeamsaltConstants.LOGIN_USER);
        redirect("/login");
    }

    @Before(POST.class)
    @ClearInterceptor(ClearLayer.ALL)
    public void loginf() {
        String email = getPara("email");
        String password = PwdKit.pwdEncrypt(getPara("password"));
        User user = User.dao.login(email, password);
        if (user != null) {
            log.debug("登录成功");
            setSessionAttr(TeamsaltConstants.LOGIN_USER, user);
            redirect("/home");
        } else {
            log.debug("登录失败");
            setAttr("msg", "用户名或密码错误");
            redirect("/login");
        }
    }
}
