package com.teamsalt.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.teamsalt.common.TeamsaltConstants;

/**
 * 
 * @ClassName: GlobalLoginInterceptor
 * @Description: 全局用户登录拦截器
 * @author zhaopeng
 * @date 2013-6-21 下午2:56:25
 * 
 */
public class GlobalLoginInterceptor implements Interceptor {
    @Override
    public void intercept(ActionInvocation ai) {
        Controller controller = ai.getController();
        if (controller.getSessionAttr(TeamsaltConstants.LOGIN_USER) != null) {
            ai.invoke();
        } else {
            controller.setAttr("msg", "需要登录才可以进行改操作：）");
            controller.redirect("/login");
        }
    }
}
