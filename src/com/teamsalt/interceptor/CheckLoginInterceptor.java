package com.teamsalt.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.teamsalt.common.TeamsaltConstants;

/**
 * 
 * @ClassName: CheckLoginInterceptor
 * @Description: 当访问/login页面的时候,判断是否已经登录,已经登录,则跳转到/home
 * @author zhaopeng
 * @date 2013-6-21 下午3:04:52
 * 
 */
public class CheckLoginInterceptor implements Interceptor {

    @Override
    public void intercept(ActionInvocation ai) {
        Controller controller = ai.getController();
        if (controller.getSessionAttr(TeamsaltConstants.LOGIN_USER) != null) {
            controller.redirect("/home");
        } else {
            ai.invoke();
        }
    }

}
