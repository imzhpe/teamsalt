package com.teamsalt.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class Comment extends Model<Comment> {
	public static final Comment dao = new Comment();

	public static final String tableName = "comment";

	public static final String uuid = "uuid";
	public static final String parentUuid = "parent_uuid";
	public static final String taskUuid = "task_uuid";
	public static final String userUuid = "user_uuid";
	public static final String content = "content";
	public static final String projectUuid = "project_uuid";
	public static final String createTime = "create_time";

	public List queryCommentsByTaskUuid(String projectUuid, String taskUuid) {
		return dao.find(
				"select * from comment where project_uuid=? and task_uuid=?",
				projectUuid, taskUuid);
	}
}
