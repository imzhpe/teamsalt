package com.teamsalt.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author zhaopeng
 * @time 2013-10-12 01:13:57
 * @desc 邀请用户关系表
 * 
 */
public class InviteUser extends Model<InviteUser> {
	public static final InviteUser dao = new InviteUser();

	public static final String tableName = "invite_user";

	public static final String id = "id";
	public static final String code = "code";// 邀请码
	public static final String inviterUuid = "inviter_uuid";// 邀请人UUID
	public static final String inviteeUuid = "invitee_uuid";// 被邀请人UUID
	public static final String activeTime = "active_time";// 邀请码激活时间
	public static final String projectUuid = "project_uuid";// 邀请加入的项目uuid

}
