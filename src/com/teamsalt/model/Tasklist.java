package com.teamsalt.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class Tasklist extends Model<Tasklist> {
    public static final Tasklist dao = new Tasklist();
    public static final String UUID = "uuid";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String CREAT_UUID = "creat_uuid";
    public static final String PROJECT_UUID = "project_uuid";
    public static final String CREATE_TIME = "create_time";
    public static final String STATUS = "status";

    public List<Tasklist> getByProjectUuid(String projectUuid) {
        return dao.find("select * from tasklist where project_uuid=?", projectUuid);
    }

}
