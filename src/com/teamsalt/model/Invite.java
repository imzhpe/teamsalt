package com.teamsalt.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author zhaopeng
 * @time 2013-10-12 01:05:59
 * @desc 邀请表
 * 
 */
public class Invite extends Model<Invite> {
	public static final Invite dao = new Invite();
	
	public static final String tableName = "invite";

	public static final String id = "id";
	public static final String code = "code";// 邀请码
	public static final String inviterUuid = "inviter_uuid";// 邀请人UUID
	public static final String inviteeEmail = "invitee_email";// 被邀请人EMAIL
	public static final String inviteeName = "invitee_name";// 被邀请人名册
	public static final String inviteTime = "invite_time";// 邀请时间,当前时间
	public static final String inviteOvertime = "invite_overtime";// 邀请码过期时间
	public static final String projectUuid = "project_uuid";// 邀请加入的项目uuid
	public static final String isValid = "is_valid";// 邀请码是否生效 0:未失效 1:失效

	public static final int isValid_valid = 0;
	public static final int isValid_invalid = 1;

	public Invite queryByCode(String code) {
		return this.dao.findFirst("select * from invite where code=?", code);
	}

}
